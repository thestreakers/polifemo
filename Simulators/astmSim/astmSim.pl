#!/usr/bin/perl
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are
#  met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following disclaimer
#    in the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of the  nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#


use strict;
use warnings;
use Config::Simple;
use utf8;
use Data::Random qw(:all);
use DBI;
use Tk;
use Tk::PNG;
use Tk::Chart::Pie;
use Tk::NoteBook;
use subs qw/menubar_etal/;

#~ Global variables
my $version = "1.0 alpha";
my $programName = "Universal Host Simulator";
my $sysctrldir = "/etc/polifemo";
my $settingsfile = $sysctrldir."/"."astmSim.conf";
my $debugfile = "astmSim.debug";
my $settings = new Config::Simple(syntax=>'ini');
my ($numIncomplete, $numSent, $numReceived) = (0,0,1);
my @pieData = ( ['Received', 'Sent', 'Incomplete'], [$numReceived, $numSent, $numIncomplete], );
my $message = "Ready";
my ($debug, $numsurnames, $numnames, $totalPossiblePatients, $totalPossibleTests);

#~ Variables for DB connection
my ($dbh, $dbd, $rc, $rs, $rv, $sth, $connected, $trace_level, $mysql_trace, $serverip, $serverport, $serverschema, $username, $userpassword, $refreshtime, $query);

#~ Variables for Middleware connection
my ($midipaddr, $midch1, $midch2, $midch3, $midnumchannel, $lisConnected);

#~ Variables for Operations
my ($workNumPatients, $workNumTest, $workStartSid, $barcode, $patname, $patsurname, $getname, $getsurname, $birthdate, $getbirthdate, $pid, $sex, $getpatientsex, $drawdate, $totNumWards, $workNumWards, $codward);
my (@arrayNames, @arraySurnames, @arrayWards, @randarrayWard, @arrayTests, @arrayTestSelected);

#~ Graphical elements
#~ Menu Bar creation and display
my $mw = MainWindow->new( -title => $programName );
$mw->minsize(300, 450);
my $menubar = $mw->Menu( -menuitems => menubar_etal );
$mw->configure( -menu => $menubar );

#~ Image icon path and Global variables
my $okbutton 	= $mw->Photo( -file => 'pix/button_ok.png', -width => '22', -height => '22');
my $faultbutton = $mw->Photo( -file => 'pix/button_cancel.png', -width => '22', -height => '22');
my $pausebutton = $mw->Photo( -file => 'pix/pause.png', -width => '22', -height => '22');
my $repeatbutton= $mw->Photo( -file => 'pix/repeat-1.png', -width => '22', -height => '22');

#~ GraphicalFrame in top left position
my $graphFrame 		= $mw->Frame( -label => 'Graph', -borderwidth =>4, -relief => 'groove');
my $graphPainter 	= $graphFrame->Canvas();
my $chart 			= $graphPainter->Pie( -title=> 'Requests status: ', -linewidth => 2);
$chart	->	enabled_gradientcolor();
$chart	->	set_gradientcolor( -start_color => '#6585ED', -end_color   => '#FFFFFF', );

#~ Status Frame in top right position
my $optionsFrame 		= $mw->Frame( -label => 'Status', -borderwidth =>4, -relief => 'groove');
my $dbconnectionFrame	= $optionsFrame->Frame();
my $dbconnectionLabel 	= $dbconnectionFrame->Label( -text =>'DB Connection', -borderwidth=>0, -anchor=>'w', -relief=>'groove', -state=>'disabled');
my $dbconnectionPic 	= $dbconnectionFrame->Button( -image => $pausebutton, -anchor=>'e', -relief => 'groove', -borderwidth => 0, -command => \&connectDB );
my $mwconnectionFrame	= $optionsFrame->Frame();
my $lisconnectionLabel 	= $mwconnectionFrame->Label( -text =>'MW Connection', -borderwidth=>0, -anchor=>'w', -relief=>'groove', -state=>'disabled');
my $lisconnectionPic 	= $mwconnectionFrame->Button( -image => $pausebutton, -anchor=>'e', -relief => 'groove', -borderwidth => 0, -command => \&connectLIS );
my $dbnumsurnameFrame	= $optionsFrame->Frame();
my $dbnumsurnameLabel	= $dbnumsurnameFrame->Label( -text =>'N° Surnames', -borderwidth=>0, -anchor=>'w', -relief=>'groove', -state=>'disabled');
my $dbnumsurnameEntry 	= $dbnumsurnameFrame->Entry( -textvariable => \$numsurnames, -borderwidth => 0, -justify => 'right', -relief =>'groove');
my $dbnumnameFrame		= $optionsFrame->Frame();
my $dbnumnameLabel		= $dbnumnameFrame->Label( -text =>'N° Names', -borderwidth=>0, -anchor=>'w', -relief=>'groove', -state=>'disabled');
my $dbnumnameEntry		= $dbnumnameFrame->Entry( -textvariable => \$numnames, -borderwidth => 0, -justify => 'right', -relief =>'groove');
my $dbpossibleFrame		= $optionsFrame->Frame();
my $dbpossibleLabel		= $dbpossibleFrame->Label( -text => 'Tot. Patients combination', -borderwidth=>0, -anchor=>'w', -relief=>'groove', -state=>'disabled');
my $dbpossibleEntry		= $dbpossibleFrame->Entry( -textvariable => \$totalPossiblePatients, -borderwidth => 0, -justify => 'right', -relief =>'groove');
my $dbtotaltestFrame	= $optionsFrame->Frame();
my $dbtotaltestLabel	= $dbtotaltestFrame->Label( -text => 'Tot. Test Configured', -borderwidth=>0, -anchor=>'w', -relief=>'groove', -state=>'disabled');
my $dbtotaltestEntry	= $dbtotaltestFrame->Entry( -textvariable => \$totalPossibleTests, -borderwidth => 0, -justify => 'right', -relief =>'groove');
my $dbtotalwardFrame	= $optionsFrame->Frame();
my $dbtotalwardLabel 	= $dbtotalwardFrame->Label( -text => 'Tot. Ward Presents', -borderwidth=>0, -anchor=>'w', -relief=>'groove', -state=>'disabled');
my $dbtotalwardEntry	= $dbtotalwardFrame->Entry( -textvariable => \$totNumWards, -borderwidth => 0, -justify => 'right', -relief =>'groove');
my $dbcreatewardFrame	= $optionsFrame->Frame();
my $dbcreatewardLabel	= $dbcreatewardFrame->Label( -text => 'N° Wards to use', -borderwidth=>0, -anchor=>'w', -relief=>'groove', -state=>'disabled');
my $dbcreatewardEntry	= $dbcreatewardFrame->Entry( -textvariable => \$workNumWards, -borderwidth => 0, -justify => 'right', -relief =>'groove');
my $dbpatrequestFrame	= $optionsFrame->Frame();
my $dbpatrequestLabel	= $dbpatrequestFrame->Label( -text => 'N° Patients to create', -borderwidth=>0, -anchor=>'w', -relief=>'groove', -state=>'disabled');
my $dbpatrequestEntry	= $dbpatrequestFrame->Entry( -textvariable => \$workNumPatients, -borderwidth => 0, -justify => 'right', -relief =>'groove');
my $dbtestrequestFrame	= $optionsFrame->Frame();
my $dbtestrequestLabel	= $dbtestrequestFrame->Label( -text => 'N° Test to create', -borderwidth=>0, -anchor=>'w', -relief=>'groove', -state=>'disabled');
my $dbtestrequestEntry	= $dbtestrequestFrame->Entry( -textvariable => \$workNumTest, -borderwidth => 0, -justify => 'right', -relief =>'groove');
my $dbstartsidFrame		= $optionsFrame->Frame();
my $dbstartsidLabel		= $dbstartsidFrame->Label( -text =>'Starting SID', -borderwidth=>0, -anchor=>'w', -relief=>'groove', -state=>'disabled');
my $dbstartsidEntry		= $dbstartsidFrame->Entry( -textvariable => \$workStartSid, -borderwidth => 0, -justify => 'right', -relief =>'groove');
my $dbcreatepatFrame	= $optionsFrame->Frame();
my $dbcreatepatLabel	= $dbcreatepatFrame->Label( -text =>'Create / Recreate patients', -borderwidth=>0, -anchor=>'w', -relief=>'groove', -state=>'disabled');
my $dbcreatepatPic		= $dbcreatepatFrame->Button( -image => $repeatbutton, -anchor=>'e', -relief => 'groove', -borderwidth => 0, -command => \&createPatients );

#~ Commands Frame in bottom down position
my $bottomFrame 	= $mw->Frame( -label => 'Commands', -borderwidth => 4, -relief => 'raised');
my $quitButton 		= $bottomFrame->Button( -text => 'Quit', -command => \&exitProgram);
my $infoPane		= $bottomFrame->Label( -anchor =>'center', -textvariable => \$message, -borderwidth => 2, -relief => 'groove');
my $startStopButton = $bottomFrame->Button( -text => 'Start / Stop');

#~ Packing Graphical elements
#~ Packing Frames
$bottomFrame		->	pack( -anchor => 's', -side => 'bottom', -fill => 'x');
$optionsFrame		->	pack( -anchor => 'ne', -side => 'right', -fill => 'y');
$dbconnectionFrame	->	pack( -anchor => 'center', -fill => 'x');
$mwconnectionFrame	->	pack( -anchor => 'center', -fill => 'x');
$dbnumsurnameFrame	->	pack( -anchor => 'center', -fill => 'x');
$dbnumnameFrame		->	pack( -anchor => 'center', -fill => 'x');
$dbpossibleFrame	->	pack( -anchor => 'center', -fill => 'x');
$dbtotaltestFrame	->	pack( -anchor => 'center', -fill => 'x');
$dbtotalwardFrame	->	pack( -anchor => 'center', -fill => 'x');
$dbcreatewardFrame	->	pack( -anchor => 'center', -fill => 'x');
$dbpatrequestFrame	->	pack( -anchor => 'center', -fill => 'x');
$dbtestrequestFrame	->	pack( -anchor => 'center', -fill => 'x');
$dbstartsidFrame	->	pack( -anchor => 'center', -fill => 'x');
$dbcreatepatFrame	->	pack( -anchor => 'center', -fill => 'x');
$graphFrame			->	pack( -anchor => 'nw', -side => 'left', -fill => 'y');

#~ Packing Graph Frame
$graphPainter	->	pack( -anchor => 'center', -fill => 'both');
$chart			->	pack( -fill => 'both', -expand => 0);

#~ Packing Option Frame
$dbconnectionLabel	->	pack( -expand => 1, -fill => 'x', -anchor => 'center', -side => 'left' );
$dbconnectionPic	->	pack( -after=> $dbconnectionLabel, -expand => 1, -fill => 'x', -anchor => 'e', -side => 'right');
$lisconnectionLabel	->	pack( -expand => 1, -fill => 'x', -anchor => 'center', -side => 'left' );
$lisconnectionPic	->	pack( -after => $lisconnectionLabel, -expand => 1, -fill => 'x', -anchor => 'center', -side => 'right');
$dbnumsurnameLabel	->	pack( -expand => 1, -fill => 'x', -anchor => 'center', -side => 'left' );
$dbnumsurnameEntry	->	pack( -after=> $dbnumsurnameLabel, -expand => 1, -fill => 'x', -anchor => 'center', -side => 'right');
$dbnumnameLabel		->	pack( -expand => 1, -fill => 'x', -anchor => 'center', -side => 'left' );
$dbnumnameEntry		->	pack( -after=> $dbnumnameLabel, -expand => 1, -fill => 'x', -anchor => 'center', -side => 'right');
$dbpossibleLabel	->	pack( -expand => 1, -fill => 'x', -anchor => 'center', -side => 'left' );
$dbpossibleEntry	->	pack( -after=> $dbpossibleLabel, -expand => 1, -fill => 'x', -anchor => 'center', -side => 'right');
$dbtotaltestLabel	->	pack( -expand => 1, -fill => 'x', -anchor => 'center', -side => 'left' );
$dbtotaltestEntry	->	pack( -after=> $dbtotaltestLabel, -expand => 1, -fill => 'x', -anchor => 'center', -side => 'right');
$dbtotalwardLabel	->	pack( -expand => 1, -fill => 'x', -anchor => 'center', -side => 'left' );
$dbtotalwardEntry	->	pack( -after=> $dbtotalwardLabel, -expand => 1, -fill => 'x', -anchor => 'center', -side => 'right');
$dbcreatewardLabel	->	pack( -expand => 1, -fill => 'x', -anchor => 'center', -side => 'left' );
$dbcreatewardEntry	->	pack( -after=> $dbcreatewardLabel, -expand => 1, -fill => 'x', -anchor => 'center', -side => 'right');
$dbpatrequestLabel	->	pack( -expand => 1, -fill => 'x', -anchor => 'center', -side => 'left' );
$dbpatrequestEntry	->	pack( -after=> $dbpatrequestLabel, -expand => 1, -fill => 'x', -anchor => 'center', -side => 'right');
$dbtestrequestLabel	->	pack( -expand => 1, -fill => 'x', -anchor => 'center', -side => 'left' );
$dbtestrequestEntry	->	pack( -after=> $dbtestrequestLabel, -expand => 1, -fill => 'x', -anchor => 'center', -side => 'right');
$dbstartsidLabel	->	pack( -expand => 1, -fill => 'x', -anchor => 'center', -side => 'left' );
$dbstartsidEntry	->	pack( -after=> $dbstartsidLabel, -expand => 1, -fill => 'x', -anchor => 'center', -side => 'right');
$dbcreatepatLabel	->	pack( -expand => 1, -fill => 'x', -anchor => 'center', -side => 'left' );
$dbcreatepatPic		->	pack( -after=> $dbcreatepatLabel, -expand => 1, -fill => 'x', -anchor => 'center', -side => 'right');

#~ Packing Commands Frame
$startStopButton	->	pack( -expand => 0, -anchor => 'sw', -side => 'left' );
$infoPane			->	pack( -after =>$startStopButton, -expand => 1, -fill => 'x', -side =>'left');
$quitButton			->	pack( -after => $startStopButton, -expand => 0, -anchor =>'se', -side => 'right' );

$chart	->	enabled_automatic_redraw;
$chart->plot(\@pieData);
MainLoop();

sub menubar_etal {
	[
		map ['cascade',  $_->[0], -menuitems => $_->[1]],
			['~File',
				[
					[qw/command/, '~Open', qw/-accelerator/, 'Ctrl-o', qw/-command/ => \&OpenTrace],               '',
					[qw/command/, '~Save', qw/-accelerator/, 'Ctrl-s', qw/-command/ => \&SaveTrace],
					[qw/command/, 'S~ave As...', qw/-accelerator/, 'Ctrl-a', qw/-command/ => \&SaveTraceAs], '',
					[qw/command/, '~Quit', qw/-accelerator/, 'Ctrl-q', qw/-command/ => \&exitProgram],
				],
			],
			['~Edit',
				[
					[qw/command/, '~Preferences', qw/-accelerator/, 'Ctrl-p', qw/-command/ => \&editPref], '',
					[qw/command/, '~Load Settings', qw/-accelerator/, 'Ctrl-l', qw/-command/ => \&loadSettings],
				],
			],
			['~Help',
				[
					[qw/command/, '~Version', qw/-accelerator/, 'Ctrl-v', qw/-command/ => \&showVersion], '',
					[qw/command/, 'Abo~ut', qw/-accelerator/, 'Ctrl-u', qw/-command/ => \&showAbout], '',
					[qw/command/, '~Credits', qw/-accelerator/, 'Ctrl-c', qw/-command/ => \&showCredits],
				],
			],
	];
} # end menubar_etal

sub connectLIS(){
	my $connectLISstatus = &lis_connect();
	$lisConnected = $connectLISstatus;
	if ( $connectLISstatus == 0 ){
		$lisconnectionPic -> configure( -image => $pausebutton );
		&lis_disconnect();
	} elsif ( $connectLISstatus == 1 ){
		$lisconnectionPic -> configure( -image => $okbutton );
		$message = "Connected to MiddleWare";
	} elsif ( $connectLISstatus == 2 ){
		$lisconnectionPic -> configure( -image => $faultbutton );
		$message = "MiddleWare connection Error";
	}
}

sub connectDB(){
	my $connectDBstatus = &mysql_connect();
	$connected = $connectDBstatus;
	if ( $connectDBstatus == 0 ){
		$dbconnectionPic -> configure( -image => $pausebutton );
		&mysql_disconnect();
	} elsif ( $connectDBstatus == 1 ){
		$dbconnectionPic -> configure( -image => $okbutton );
		$message = "Connected to Database";
		$numsurnames = &retrieveSurnames();
		$numnames = &retrieveNames();
		$totNumWards = &retrieveNumWards();
		$totalPossiblePatients = $numsurnames * $numnames if (($numsurnames >= 0) && ($numnames >= 0));
		$totalPossibleTests = &retrieveNumTests();
		&refreshGraph();
	} elsif ( $connectDBstatus == 2 ){
		$dbconnectionPic -> configure( -image => $faultbutton );
		$message = "Database Connection Error";
	}
}

sub refreshGraph(){
	$numIncomplete = &retrieveIncomplete();
	$numSent = &retrieveSent();
	$numReceived = &retrieveReceived();
	$numIncomplete = 1 if (($numIncomplete == 0) && ($numReceived == 0) && ($numSent == 0));
	@pieData = ( ['Received $numReceived', 'Sent', 'Incomplete'], [$numReceived, $numSent, $numIncomplete], );
	&debugMessage("Received:$numReceived Sent:$numSent Incomplete:$numIncomplete") if ($debug);
	$chart	->	clearchart();
	$chart	->	plot(\@pieData);
	$chart	->	redraw();
}

sub createPatients(){
	if ( $connected ){
		my($lenArrayNames) = $#arrayNames;
		if ($lenArrayNames < 0){
			$query = "SELECT name FROM names";
			&sql($query);
			while (my $element = $sth->fetchrow()){
				push @arrayNames, $element;
			}
			&debugMessage("Name array has $#arrayNames elements") if ($debug);
		}
		my($lenArraySurname) = $#arraySurnames;
		if ($lenArraySurname < 0){
			$query = "SELECT surname FROM surnames";
			&sql($query);
			while (my $element2 = $sth->fetchrow()){
				push @arraySurnames, $element2;
			}
			&debugMessage("Surame array has $#arraySurnames elements") if ($debug);
		}
		my($lenArrayWards) = $#arrayWards;
		if ($lenArrayWards < 0){
			$query = "SELECT codward FROM wards";
			&sql($query);
			while (my $element3 = $sth->fetchrow()){
				push @arrayWards, $element3;
			}
			&debugMessage("Wards array has $#arrayWards elements") if ($debug);
		}
		my($lenArrayTests) = $#arrayTests;
		if ($lenArrayTests < 0){
			$query = "SELECT assaycode from assays";
			&sql($query);
			while (my $element4 = $sth->fetchrow()){
				push @arrayTests, $element4;
			}
			&debugMessage("Tests array has $#arrayTests elements") if ($debug);
		}
		$query = "SELECT COUNT(*) FROM patients";
		&sql($query);
		my($counter) = $sth->fetchrow();
		if ($counter == 0){
			&debugMessage("No previous Patients found->$counter") if ($debug);
			$barcode = $workStartSid;
			for(my $idx = 1; $idx <= $workNumPatients; $idx++){
				$patsurname = &getPatSurname(@arraySurnames);
				$patname = &getPatName(@arraySurnames);
				$birthdate = &getPatBirthdate();
				$sex = &getPatSex();
				$drawdate = rand_datetime(min => '1970-01-01', max => 'now');
				$codward = &getWardCode(@arrayWards);
				$pid = &getCodPatientId($patsurname, $patname, $birthdate, $sex);
				my @arraySelectedTests = &getTestCodes(@arrayTests);
				foreach my $testcode (@arraySelectedTests){
					&debugMessage("Creating Patient N° $idx SID->$barcode Surname->$patsurname Name->$patname Birth->$birthdate SEX->$sex PID->$pid DATEDRAW->$drawdate WARD->$codward TEST->$testcode") if ($debug);
					$message = "Inserting Patient N° $idx of $workNumPatients Barcode $barcode";
					&insertNewPatient($barcode, $patsurname, $patname, $birthdate, $sex, $pid, $drawdate, $codward, $testcode);
					&refreshGraph();
				}
				$barcode++;
			}
			$message = "Ready";
		} else {
			&debugMessage("There are already $counter patients") if ($debug);
			$query = "TRUNCATE TABLE patients";
			&sql($query);
			$barcode = $workStartSid;
			for(my $idx = 1; $idx <= $workNumPatients; $idx++){
				$patsurname = &getPatSurname(@arraySurnames);
				$patname = &getPatName(@arraySurnames);
				$birthdate = &getPatBirthdate();
				$sex = &getPatSex();
				$drawdate = rand_datetime(min => '1970-01-01', max => 'now');
				$codward = &getWardCode(@arrayWards);
				$pid = &getCodPatientId($patsurname, $patname, $birthdate, $sex);
				my @arraySelectedTests = &getTestCodes(@arrayTests);
				foreach my $testcode (@arraySelectedTests){
					&debugMessage("Creating Patient N° $idx SID->$barcode Surname->$patsurname Name->$patname Birth->$birthdate SEX->$sex PID->$pid DATEDRAW->$drawdate WARD->$codward TEST->$testcode") if ($debug);
					$message = "Inserting Patient N° $idx of $workNumPatients Barcode $barcode";
					&insertNewPatient($barcode, $patsurname, $patname, $birthdate, $sex, $pid, $drawdate, $codward, $testcode);
					&refreshGraph();
				}
				$barcode++;
			}
			$message = "Ready";
		}
	}
}

sub insertNewPatient(){
	my($sid, $surname, $name, $datebirth, $sex, $patientid, $datedraw, $wardcode, $codtest) = @_;
	$query = "INSERT INTO PATIENTS (codpid, barcode, name, surname, birthdate, sex, drawdate, codward, codtest, flgstatus, txtresult1, txtresult2, txtresult1bk1, txtresult2bk1) VALUES (\"$patientid\", \"$sid\", \"$name\", \"$surname\", \"$datebirth\", \"$sex\", \"$datedraw\", \"$wardcode\", \"$codtest\", \"I\", \"\", \"\", \"\", \"\" )";
	&sql($query);
}

sub getTestCodes(){
	my(@testlist) = @_;
	@arrayTestSelected = ();
	for (my $index = 1; $index <= $workNumTest; $index++){
		my($testselected) = int(rand($totalPossibleTests));
		push @arrayTestSelected, $testlist[$testselected];
	}
	return(@arrayTestSelected);
}

sub getCodPatientId(){
	my($surname, $name, $birthdate, $sex) = @_;
	$surname =~ s/\s+//g;
	$name =~ s/\s+//g;
	$birthdate =~ s/\-//g;
	$pid = $surname.$name.$birthdate.$sex;
}

sub getPatSex(){
	my(@patsex) = ('M', 'F', 'U');
	my($idx) = int(rand(3));
	$getpatientsex = $patsex[$idx];
	return($getpatientsex);
}

sub getPatBirthdate(){
	$getbirthdate = rand_date( min => '1900-1-01', max => 'now' );
	return($getbirthdate);
}

sub getPatSurname(){
	my(@surnames) = @_;
	my($index) = int(rand($#surnames+1));
	$getsurname = splice @surnames, $index, 1;
	return($getsurname);
}

sub getPatName(){
	my(@names) = @_;
	my($index) = int(rand($#names+1));
	$getname = splice @names, $index, 1;
	return($getname);
}

sub getWardCode(){
	my(@wards) = @_;
	my $lenRandArrayWard = $#randarrayWard;
	if ($lenRandArrayWard == 0){
		for (my $idx = 1; $idx <= $workNumWards; $idx++){
			my $pointer = int(rand($workNumWards));
			push @randarrayWard, $wards[$pointer];
		}
		$lenRandArrayWard = $#randarrayWard;
		for (my $idy = 1; $idy <= $lenRandArrayWard; $idy++){
			my $pointerx = int(rand($lenRandArrayWard));
			$codward = $wards[$pointerx];
		}
	} else {
		my $pointerx = int(rand($lenRandArrayWard));
		$codward = $wards[$pointerx];
	}
}

sub retrieveNumWards(){
	$query = "SELECT COUNT(idward) FROM wards";
	&sql($query);
	my($counter) = $sth->fetchrow();
	$totNumWards = $counter;
}

sub retrieveNumTests(){
	$query = "SELECT COUNT(assaycode) FROM assays";
	&sql($query);
	my($counter) = $sth->fetchrow();
	$totalPossibleTests = $counter;
}

sub retrieveNames(){
	$query = "SELECT COUNT(name) FROM names";
	&sql($query);
	my($counter) = $sth->fetchrow();
	$numnames = $counter;
}

sub retrieveSurnames(){
	$query = "SELECT COUNT(surname) FROM surnames";
	&sql($query);
	my($counter) = $sth->fetchrow();
	$numsurnames = $counter;
}

sub retrieveIncomplete(){
	$query = "SELECT COUNT(DISTINCT(barcode)) FROM patients WHERE flgstatus = 'I' AND txtresult1 = '' AND txtresult2 = '' ";
	&sql($query);
	my($incomplete) = $sth->fetchrow();
	$numIncomplete = $incomplete;
}

sub retrieveSent(){
	$query = "SELECT COUNT(DISTINCT(barcode)) FROM patients WHERE flgstatus = 'S' ";
	&sql($query);
	my($sent) = $sth->fetchrow();
	$numSent = $sent;
}

sub retrieveReceived(){
	$query = "SELECT COUNT(DISTINCT(barcode)) FROM patients WHERE flgstatus = 'R' ";
	&sql($query);
	my($received) = $sth->fetchrow();
	$numReceived = $received;
}

#~ sub retrieveTotal(){
	#~ $query = "SELECT COUNT(*) FROM patients";
	#~ &sql($query);
	#~ my($total) = $sth->fetchrow();
	#~ $numTotal = $total;
#~ }

sub sql() {
	my($query) = @_;
	$message = ("EXECUTING QUERY->$query") if ($debug);
    $sth 	= $dbh->prepare("/*user=$username*/".$query);
    $rv		= $sth->execute;
    $rc 	= $sth->err;
	$rs 	= $sth->errstr;
    if ($rc){
		&debugMessage("SQLERR->$query->$rc->$rs");
	}
}

sub mysql_connect(){
	if ($connected){
		&debugMessage("Already connected") if ($debug);
		$connected = 0;
		return 0;
	} elsif (($serverschema) && ($serverip) && ($serverport) && ($username) && ($userpassword)) {
		$message = ("Connecting to MySql server");
		&debugMessage("Connecting with: SCHEMA->$serverschema IP->$serverip PORT->$serverport User->$username Password->$userpassword") if ($debug);
		$dbd = "DBI:mysql:".$serverschema.";host=".$serverip.";port=".$serverport;
		$dbh = DBI->connect($dbd, $username, $userpassword, {RaiseError => 0});
		$dbh->trace($trace_level, $mysql_trace) if ($debug);
		return 1 if ($dbh);
		return 2;
	} else {
		$mw -> messageBox( -title => "Warning", -message => "Settings not loaded");
		return 0;
	}
}

sub mysql_disconnect(){
	if ($dbh){
		$dbh->disconnect();
	}
	&debugMessage("MySql connection closed") if ($debug);
}

sub lis_connect(){
	if ($lisConnected){
		&debugMessage("MiddleWare already Connected") if ($debug);
		$lisConnected = 0;
		return 0;
	} 
	
}

#~ Create a topLevel Panel to store settings in the configuration file
sub editPref(){
	$message = "Edit Preferences...";
	if ( -e $settingsfile ){
		&loadSettings();
	}
	my $prefWin 		= $mw->Toplevel( -title => 'Preferences', -height => 400, -width => 250 );
	$prefWin->packPropagate(0);
	my $prefNotebook	= $prefWin->NoteBook()																			->pack( -expand => 1, -fill => 'both');
	my $appPagePref 	= $prefNotebook->add( 'appPage', -label => 'Application');
	my $lblserverip 	= $appPagePref->Label( -text => 'Server Address', -anchor => 'center', -relief => 'flat')		->pack();
	my $entserverip 	= $appPagePref->Entry( -textvariable => \$serverip, -relief => 'sunken')							->pack();
	my $lblserverport	= $appPagePref->Label( -text => 'Server Port', -anchor => 'center', -relief => 'flat')			->pack();
	my $entserverport	= $appPagePref->Entry( -textvariable => \$serverport, -relief => 'sunken')						->pack();
	my $lblserverschema = $appPagePref->Label( -text => 'DB Name', -anchor => 'center', -relief => 'flat')				->pack();
	my $entserverschema = $appPagePref->Entry( -textvariable => \$serverschema, -relief =>'sunken')						->pack();
	my $lblusername 	= $appPagePref->Label( -text => 'Username', -anchor => 'center', -relief => 'flat')				->pack();
	my $entusername 	= $appPagePref->Entry( -textvariable => \$username, -relief =>'sunken')							->pack();
	my $lbluserpassword = $appPagePref->Label( -text => 'Password', -anchor => 'center', -relief => 'flat')				->pack();
	my $entuserpassword = $appPagePref->Entry( -textvariable => \$userpassword, -relief =>'sunken')						->pack();
	my $lblrefreshtime 	= $appPagePref->Label( -text => 'Refresh (sec.)', -anchor => 'center', -relief => 'flat')		->pack();
	my $entrefreshtime 	= $appPagePref->Entry( -textvariable => \$refreshtime, -relief =>'sunken')						->pack();
	my $lbldebug		= $appPagePref->Label( -text => 'Debug', -anchor => 'center', -relief => 'flat')					->pack();
	my $entdebug		= $appPagePref->Entry( -textvariable => \$debug, -relief =>'sunken')								->pack();
	my $connPagePref	= $prefNotebook->add( 'connPage', -label => 'Connection');
	my $lblMidIpAddress	= $connPagePref->Label( -text => 'Mid. IP address', -anchor => 'center', -relief => 'flat')		->pack();
	my $entMidIpAddress	= $connPagePref->Entry( -textvariable => \$midipaddr, -relief => 'sunken')						->pack();
	my $lblch1port		= $connPagePref->Label( -text => 'Order channel port', -anchor => 'center', -relief => 'flat')	->pack();
	my $entch1port		= $connPagePref->Entry( -textvariable => \$midch1, -relief => 'sunken')							->pack();
	my $lblch2port		= $connPagePref->Label( -text => 'Result channel port', -anchor => 'center', -relief => 'flat')	->pack();
	my $entch2port		= $connPagePref->Entry( -textvariable => \$midch2, -relief => 'sunken')							->pack();
	my $lblch3port		= $connPagePref->Label( -text => 'Track channel port', -anchor => 'center', -relief => 'flat')	->pack();
	my $entch3port		= $connPagePref->Entry( -textvariable => \$midch3, -relief => 'sunken')							->pack();
	my $lblnumchan		= $connPagePref->Label( -text => 'Num Channels', -anchor => 'center', -relief => 'flat')			->pack();
	my $entnumchan		= $connPagePref->Entry( -textvariable => \$midnumchannel, -relief => 'sunken')					->pack();
	my $operPagePref	= $prefNotebook->add( 'operPage', -label => 'Operations');
	my $lblNumPatient	= $operPagePref->Label( -text => 'Patient to send', -anchor => 'center', -relief => 'flat')		->pack();
	my $entNumPatient	= $operPagePref->Entry( -textvariable => \$workNumPatients, -relief => 'sunken')					->pack();
	my $lblNumTest		= $operPagePref->Label( -text => 'Max Test to create', -anchor => 'center', -relief => 'flat')	->pack();
	my $entNumTest		= $operPagePref->Entry( -textvariable => \$workNumTest, -relief => 'sunken')						->pack();
	my $lblStartSid		= $operPagePref->Label( -text => 'Starting SID', -anchor => 'center', -relief => 'flat')			->pack();
	my $entStartSid		= $operPagePref->Entry( -textvariable => \$workStartSid, -relief => 'sunken')					->pack();
	my $lblNumWards		= $operPagePref->Label( -text => 'Max Ward to create', -anchor => 'center', -relief => 'flat')	->pack();
	my $entNumWards		= $operPagePref->Entry( -textvariable => \$workNumWards, -relief => 'sunken')					->pack();
	my $sbprefWin 		= $prefWin->Button( -text => 'Save', -command => sub {
																				save_prefs(
																					$serverip,
																					$serverport,
																					$serverschema,
																					$username,
																					$userpassword,
																					$refreshtime,
																					$debug,
																					$midipaddr,
																					$midch1,
																					$midch2,
																					$midch3,
																					$midnumchannel,
																					$workNumPatients,
																					$workNumTest,
																					$workStartSid,
																					$workNumWards
																				);
																			}
											)																			->pack();
	my $qbprefWin 		= $prefWin->Button( -text => 'Close', -command => sub {close_win($prefWin);} )					->pack();
}

sub save_prefs(){
	my($ip, $port, $schema, $user, $password, $reftime, $debug, $midip, $midch1port, $midch2port, $midch3port, $midchnum, $workNumPatients, $workNumTest, $workStartSid, $workNumWards) = @_;
	$settings->param("server.address", $ip);
	$settings->param("server.port", $port);
	$settings->param("server.schema", $schema);
	$settings->param("server.user", $user);
	$settings->param("server.password", $password);
	$settings->param("service.refresh", $reftime);
	$settings->param("service.debug", $debug);
	$settings->param("middleware.address", $midip);
	$settings->param("middleware.orderport", $midch1port);
	$settings->param("middleware.resultport", $midch2port);
	$settings->param("middleware.trackport", $midch3port);
	$settings->param("middleware.numchannels", $midchnum);
	$settings->param("operations.numpatients", $workNumPatients);
	$settings->param("operations.numtests", $workNumTest);
	$settings->param("operations.startsid", $workStartSid);
	$settings->param("operations.numwards", $workNumWards);
	$settings->write($settingsfile);
	$mw->messageBox( -title => $programName." ".$version, -message => "Settings saved");
	&loadSettings();
}

sub loadSettings(){
	if ( -e $settingsfile ){
		$settings 		= new Config::Simple($settingsfile);
		$serverip 		= $settings->param("server.address");
		$serverport		= $settings->param("server.port");
		$serverschema 	= $settings->param("server.schema");
		$username 		= $settings->param("server.user");
		$userpassword 	= $settings->param("server.password");
		$refreshtime 	= $settings->param("service.refresh");
		$debug			= $settings->param("service.debug");
		$midipaddr		= $settings->param("middleware.address");
		$midch1			= $settings->param("middleware.orderport");
		$midch2			= $settings->param("middleware.resultport");
		$midch3			= $settings->param("middleware.trackport");
		$midnumchannel	= $settings->param("middleware.numchannels");
		$workNumPatients= $settings->param("operations.numpatients");
		$workNumTest	= $settings->param("operations.numtests");
		$workStartSid	= $settings->param("operations.startsid");
		$workNumWards	= $settings->param("operations.numwards");
		$mw->messageBox( -title => $programName." ".$version, -message => "Settings loaded" );
	} else {
		$mw->messageBox( -title => $programName." ".$version, -message => "No settings found. Please create new ones!" );
	}
}

sub exitProgram(){
	$mw->messageBox( -title => $programName." ".$version, -message=>"Goodbye!");
	exit;
}

sub close_win(){
	my $thisWin = $_[0];
	$thisWin->destroy;
	$message = "Ready";
}

sub debugMessage(){
	my ($logmessage, $dest) = @_;
	if (defined $dest){
		open(OUT, ">>$debugfile");
		print OUT localtime()." $logmessage\n";
		close(OUT);
	} else {
		$mw->messageBox( -title => "DEBUG", -message => $logmessage );
	}
}

sub showVersion(){
	$mw->messageBox( -title => $programName." ".$version, -message=>$programName." ".$version );
}

sub showAbout(){
	$mw->messageBox( -title => $programName." ".$version, -message=>"This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.");
}

sub showCredits(){
	$mw->messageBox( -title => $programName." ".$version, -message=>" Icons provided by -> Freepik and distributed by Flaticon");
}
