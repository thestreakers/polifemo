#!/usr/bin/perl

#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are
#  met:
#  
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following disclaimer
#    in the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of the  nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  


use strict;
use warnings;
use utf8;
use DBI;

#~ global variables
my $progname = "dbloader";
my $configfile = $progname.".ini";
my $debug;
my $trace_level = 4;
my $mysql_trace = "dbtrace.txt";
my @schemas;
my $match;
my $elem;

#~ variables for files with data
my $namesfile = "dbnames.txt";
my $surnamesfile = "dbsurnames.txt";
my $testfile = "Assay.txt";
my $wardfile = "ward.txt";

#~ global variables for db connection
my ($host, $username, $password, $schema, $dbh, $connected, $rs, $rc, $rv, $sth, $query, $dbd);
$schema = "astmsim";

if (($#ARGV >= 0) && ($#ARGV <= 3)){
	$host 		= $ARGV[0] or die(\&LogMessage("no host inserted"));
	$username	= $ARGV[1] or die(\&LogMessage("no username given"));
	$password	= $ARGV[2] or die(\&LogMessage("no password given"));
	$debug		= $ARGV[3] or $debug = 0;
} else {
	&LogMessage("No params given...");
	&LogMessage("Usage: $progname\t[host]\t[username]\t[password]\t[debug]");
	exit(0);
}

&LogMessage("Using-> host:$host\tuser:$username\tpassword:$password\tdebug:$debug");
if ((-e $namesfile) && (-e $surnamesfile) && (-e $testfile) && (-e $wardfile)){
	$connected = &CONNECTMYSQL();
	&LogMessage("Connected...") if (($debug) && ($connected));
	&LogMessage("Checking for default schema...");
	$match = &CheckSchema($schema);
	if ($match){
		&LogMessage("Schema found...");
		$query = "USE $schema";
		&SQL($query);
		&CreateTables();
		&PopulateTables($namesfile, $surnamesfile, $testfile);
	} else {
		&LogMessage("Schema not found... creating a new one");
		$query = "CREATE DATABASE $schema";
		&SQL($query);
		$query = "USE $schema";
		&SQL($query);
		&CreateTables();
		&PopulateTables($namesfile, $surnamesfile, $testfile);
	}
} else {
	&LogMessage("No file sources found. exiting");
	exit(0);
}

sub LogMessage(){
	my ($message) = @_;
	print localtime() . " $message\n";
}

sub CheckSchema(){
	my($schema) =@_;
	&LogMessage("Looking for $schema");
	$query = "SHOW databases";
	&SQL($query);
	while (my $name = $sth->fetchrow() ){
		push @schemas, $name;
		&LogMessage("SCHEMA FOUND->$name") if ($debug);
	}
	foreach $elem (@schemas) {
		$match = $elem if ($elem eq $schema);
	}
	return 1 if ($match);
	return 0;
}

sub CONNECTMYSQL(){
	&LogMessage("Connecting to MySql server");
	$dbd = "DBI:mysql:;host=".$host;
	$dbh = DBI->connect($dbd, $username, $password, {RaiseError => 1});
	$dbh->trace($trace_level, $mysql_trace) if ($debug);

	return 1 if ($dbh);
	return 0;
}

sub SQL {
	my($query) = @_;
	&LogMessage("EXECUTING QUERY->$query") if($debug);
    $sth = $dbh->prepare("/*user=$username*/".$query);
    $rv = $sth->execute;
    $rc = $sth->err;
	$rs = $sth->errstr;
    if ($rc){
		&LogMessage("SQLERR->$query->$rc->$rs");
	}
}

sub CreateTables(){
	$query = "DROP TABLE IF EXISTS names;";
	&LogMessage("Dropping already existing Name Table space");
	&SQL($query);
	$query = "DROP TABLE IF EXISTS surnames;";
	&LogMessage("Dropping already existing Surname Table space");
	&SQL($query);
	$query = "DROP TABLE IF EXISTS patients; ";
	&LogMessage("Dropping already existing Patients Table Space");
	&SQL($query);
	$query = "DROP TABLE IF EXISTS assays; ";
	&LogMessage("Dropping already existing Assay Table Space");
	&SQL($query);
	$query = "DROP TABLE IF EXISTS wards";
	&LogMessage("Dropping already existing Ward Table Space");
	&SQL($query);
	$query = "CREATE TABLE names ( idname INT NOT NULL AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255) NOT NULL );";
	&LogMessage("Creating Name Table space");
	&SQL($query);
	$query = "CREATE TABLE surnames ( idsurname INT NOT NULL AUTO_INCREMENT PRIMARY KEY, surname VARCHAR( 255 ) NOT NULL );";
	&LogMessage("Creatig Surname Table space");
	&SQL($query);
	$query = "CREATE TABLE assays ( idassay INT NOT NULL AUTO_INCREMENT PRIMARY KEY, assaycode VARCHAR( 20 ) NOT NULL, assaydesc VARCHAR( 255 ) NOT NULL );";
	&LogMessage("Creating Assay Table space");
	&SQL($query);
	$query = "CREATE TABLE wards( idward INT NOT NULL AUTO_INCREMENT PRIMARY KEY, codward VARCHAR( 20 ) NOT NULL, warddescription VARCHAR( 255 ) NOT NULL );";
	&LogMessage("Creating Wars Table space");
	&SQL($query);
	$query = "CREATE TABLE patients ( idrequest INT NOT NULL AUTO_INCREMENT, codpid VARCHAR( 255 ) NOT NULL, barcode VARCHAR( 20 ) NOT NULL, name VARCHAR( 255 ), surname VARCHAR( 255 ), birthdate DATETIME, sex CHAR( 1 ), drawdate DATETIME, codward VARCHAR( 20 ), codtest VARCHAR( 20 ) NOT NULL, flgstatus CHAR( 1 ), txtresult1 VARCHAR( 16 ) NOT NULL, txtresult2 VARCHAR( 16 ) NOT NULL, dateresult1 DATETIME DEFAULT NULL, txtresult1bk1 VARCHAR( 16 ) NOT NULL, txtresult2bk1 VARCHAR( 16 ) NOT NULL, dateresultbk1 DATETIME DEFAULT NULL, PRIMARY KEY( idrequest, codpid, barcode ) );";
	&LogMessage("Creating Patients Table space");
	&SQL($query);
}

sub PopulateTables(){
	my($sourcename, $sourcesurname, $sourceassay) = @_;
	my @names = ();
	my @surnames = ();
	my @assays = ();
	my @wards = ();
	$| = 1;
	open INFILE, "< $sourcename" or &LogMessage("Cannot Open $sourcename, $!");
	@names = <INFILE>;
	my $totnumrow = $#names;
	for (my $idx = 0; $idx <= $totnumrow; $idx++){
		my $value = $names[$idx];
		chomp $value;
		$value = uc($value);
		$query = "INSERT INTO names (name) VALUES (\"$value\");";
		&SQL($query);
		print localtime()." Inserted row $idx of $totnumrow\r";
	}
	print "\n".localtime()." End inserting names\n";
	close INFILE;
	open INFILE, "< $sourcesurname" or &LogMessage("Cannot Open $sourcename, $!");
	@surnames = <INFILE>;
	$totnumrow = $#surnames;
	for (my $idx = 0; $idx <= $totnumrow; $idx++){
		my $value = $surnames[$idx];
		chomp $value;
		$value = uc($value);
		$query = "INSERT INTO surnames (surname) VALUES (\"$value\");";
		&SQL($query);
		print localtime()." Inserted row $idx of $totnumrow\r";
	}
	print "\n".localtime()." End inserting surnames\n";
	close INFILE;
	open INFILE, "< $sourceassay" or &LogMessage("Cannot Open $sourceassay, $!");
	@assays = <INFILE>;
	$totnumrow = $#assays;
	for (my $idx = 0; $idx <= $totnumrow; $idx++){
		my $string = $assays[$idx];
		my($assaycode, $assaydescription) = split(/\|/, $string);
		chomp($assaycode);
		$assaycode =~ s/\s+//g;
		$assaycode = uc($assaycode);
		chomp($assaydescription);
		$query = "INSERT INTO assays (assaycode, assaydesc) VALUES (\"$assaycode\", \"$assaydescription\");";
		&SQL($query);
		print localtime()." Inserted row $idx of $totnumrow\r";
	}
	print "\n".localtime()." End inserting assays\n";
	close INFILE;
	open INFILE, "< $wardfile" or &LogMessage("Cannot Open $wardfile, $!");
	@wards = <INFILE>;
	$totnumrow = $#wards;
	for (my $idx = 0; $idx <= $totnumrow; $idx++){
		my $string = $wards[$idx];
		my($codward, $descward) = split(/\|/, $string);
		chomp $descward;
		$descward = uc($descward);
		$query = "INSERT INTO wards (codward, warddescription) VALUES (\"$codward\", \"$descward\");";
		&SQL($query);
		print localtime()." Inserted row $idx of $totnumrow\r";
	}
	print "\n".localtime()." End inserting wards\n";
	close INFILE;
}
