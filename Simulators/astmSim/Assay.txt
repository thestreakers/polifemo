FTESTO|Free Testosterone
Sgot|Alaninoaminotransferasi
Sgpt|Aspartatoaminotransferasi
SHBG|Sex Hormone Binding Globulin
Fe|Sideremia
VDRL|Sifilide
AntiDNASS|single strad singola elica
Na|Sodio
SMC|Somatomedina C
SPERMIO|Esame del liquido seminale
SCC|Squamous Cell Carcinoma
STH|Ormone della crescita
TBG|Globulina legante la tiroxina
TCD|Coombs diretto
TCI|Coombs indiretto
CARBA|Carbamazepina
PT|Tempo di protrombina
PTT|Tempo di tromboplastina parziale
TEO|Teofillina
Test di Dixon|Anti Piastrine
Test di Wright |Melitense
MONO |Test per la mononucleosi
PCT |Test post coitale
TG |Tireoglobulina HTG
TBG |Globulina legante la tiroxina
FT4 |Ft4
TPA |Tissue Polipeptide Antigen
TOXO |Toxoplasmosi
TPA |Tissue Polipeptide Antigen
TPHA |Treponema Pallidum Anticorpi
TRAB |Anti recettori del TSH
ALT |Aspartatoaminotransferasi
CDT |Transferrina carboidrato carente
TF | Transferrina insatura
TIBC |Transferrina Legata al Ferro
TPHA |Treponema Pallidum Anticorpi
FT3 |Triiodotironina Libera Free t3
AST |Alaninoaminotransferasi
TSH |Thyroid stimulating hormone
URI | Uricemia Acido urico 
AZO |Azoto ematico
URIC | Acido urico 
AHZ |Varicella Anticorpi Anti Herpes Zoster
ADH |Vasopressina antidiuretic hormone
VCA | Epstein Barr antigene
VDRL |Sifilide
VES |Velocità di eritrosedimentazione
VLDL | Very Low Density Lipoprotein
VITA | Retinolo 
VITB1 | Tiamina
VITB12 | Cobalamina
VITB2 | Riboflanina 
VITC | Aacido ascorbico 
VITE | A - tocoferolo 
VLDL | Very Low Density Lipoprotein
VMA | Acido vanilmandelico
WBC | Globuli bianchi
WFR |Reazione di Weil – Felix
XDP |Prodotti di degradazione del Fibrinogeno
Zn | Zinco