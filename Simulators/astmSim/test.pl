#!/usr/bin/perl

use strict;
use warnings;
use Encode;

my $fileinput = $ARGV[0];
my $fileoutput = $ARGV[1];

open my $in,  "<:encoding(iso-8859-1)", $fileinput  or die;
open my $out, ">:encoding(utf8)", $fileoutput or die;
while(<$in>){ print $out $_; }
