#!/usr/bin/perl
use DBI();
binmode STDOUT, ":encoding(utf8)";

my $dbh = DBI->connect('dbi:mysql:dictionaryword', 'root', 'Sabrian123',{mysql_enable_utf8=>1}) or die $DBI::errstr;
my $sth = $dbh->prepare("SELECT * FROM edict");
$sth->execute();
my @row;
while (@row = $sth->fetchrow_array()) {
	foreach $elem (@row){
		utf8::encode($elem);
	}
    print "$row[0] $row[1] $row[2] $row[3]\n";
}

$sth->finish();
$dbh->disconnect();
