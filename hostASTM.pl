#!/usr/bin/perl
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are
#  met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following disclaimer
#    in the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of the  nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

use strict;
use POSIX qw(setsid);
use Socket::Class;
use Config::Simple;
use DBI;
use Config;
$Config{useithreads} or die ('Recompile with threads to use this program');
use lib::FuncInstrument::General;

#~ Global variables
my $proc;
my $error;
my $pid;
my $file = "PoliFemo";
my $version = "0.1a";
my $debug;
my @pidrun=();
my $server;
my $oldpid;
my $cheat;

#~ Variables for DBMS Server interface
my $dbtype;
my $dbhost;
my $dbport;
my $dbschema;
my $dbuser;
my $dbpassword;
my $dbconnect;

#~ Variables for TCP/IP Server interface
my $host;
my $hostport;
my $mode;

#~ Variables fo Serial interface
my $port;
my $boudrate;
my $bitstop;
my $bitlen;
my $parity;
my $xonxoff;
