#!/usr/bin/perl

package MyEchoServer;

use strict;
use warnings;

use Net::Server::PreFork;

use base("Net::Server::PreFork");

my $self = bless { server  => { port => [ 1025 ] },
                   logfile => "/tmp/clog",
                 },"MyEchoServer";

open(my $clog,">>",$self->{logfile}) or die "Can't open $self->{logfile} : $!";

my %accept = ( ipconfig => "10.10.0.0",
                 set      => sub { $self->{value}->{$_[0]}=$_[1] },
                 get      => sub { print "$_[0] is set to $self->{value}->{$_[0]}\n" },
            );
$self->run();

sub process_request {
    my $self = shift;
    print "> ";
    while (<STDIN>) {
        chomp;
        my ($command,@param) = split;
        if (exists($accept{$command})) {
            print {$clog} "$command ".join(" ",@param)."\n"
                or die "Can't print to $self->{logfile}";
            if (ref($accept{$command}) eq "CODE") {
                $accept{$command}->(@param);
            } else {
                print $accept{$command}."\n";
           }
        } else {
            print "Don't know command $command\n";
        }
    print "> ";
    }
}
