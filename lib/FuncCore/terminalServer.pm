#!/usr/bin/perl

package terminalServer;

$VERSION = 0.0.1;

use strict;
use warnings;
use Net::Server::PreFork;
use base("Net::Server::PreFork");

my $self = bless {
	server  => { port => [ '$port' ] },
	logfile => '$logfile',
	version => '$VERSION',
},"Polifemo TerminalServer";

#~ open( OUT,">>$logfile") or die "Can't open $logfile : $!";

my %accept = (
	ipconfig	=> "10.10.0.0",
	set			=> sub { $self->{value}->{$_[0]}=$_[1] },
	get			=> sub { print "$_[0] is set to $self->{value}->{$_[0]}\n" },
	version		=> sub { print "Polifemo Terminal Server $self->{version}\n" },

);

sub process_request {
    my $self = shift;
    my $prompt = "PoliTerm>";
    print "$prompt";
    while (<STDIN>) {
        chomp;
        my ($command,@param) = split;
        if (exists($accept{$command})) {
            #~ print OUT "$command ".join(" ",@param)."\n"
                #~ or die "Can't print to $self->{logfile}";
            if (ref($accept{$command}) eq "CODE") {
                $accept{$command}->(@param);
            } else {
                print $accept{$command}."\n";
           }
        } else {
            print "Don't know command $command\n";
        }
    print "$prompt";
    }
}
1;
