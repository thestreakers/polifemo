#!/usr/bin/perl

#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are
#  met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following disclaimer
#    in the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of the  nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#~ 22.03.2014 - Andrea: First release of this file
#~ 05.04.2014 - Andrea: Adjust of log files printing and controll if the pid file is still there. Started Config file handling and socket connection for the commands.
#~ 11.05.2014 - Andrea: Solved some syntax issues
#~ 11.10.2014 - Andrea: Begin of multithreaded server programming

use strict;
use POSIX qw(setsid);
#~ use Socket::Class qw($SOMAXCONN);
use IO::Socket;
use IO::Select;
use Config::Simple;
use DBI;
use threads;
use Parallel::ForkManager;
use Net::Server::PreFork;
use base("Net::Server::PreFork");
use lib::FuncCore::terminalServer;


#~ Global variables
my $proc; my $error; my $pid; my $file = "PoliFemo"; my $version = "0.1a"; my $debug;
my @pidrun=();
my $serverproc; my $processManager; my $child;
my $oldpid;
my $cheat;
my @MainProcesses = qw(terminalServer driverManager resultManager);
my $maxMainProcesses = 3;

#~ Variables for main process
my $pidfile = ">/var/run/polifemo.pid";
my $pid2check = "/var/run/polifemo.pid";

#~ Variables for default logging path
my $syslogdir = "/var/log/polifemo";
my $logfile = "polifemo.log";
my $errorlogfile = "error.log";
my $accesslogfile = "access.log";

#~ Variables for default config path
my $sysctrldir = "/etc/polifemo";
my $configfile = $sysctrldir."/"."polifemo.conf";
my $cfgPolifemo = new Config::Simple(syntax=>'ini');
my $config;

#~ Variables for TCP/IP Server interface
my $hostserver;
my $port;
my $remoteclient;

#~ Variables for DBMS Server interface
my $dbtype;
my $dbhost;
my $dbport;
my $dbschema;
my $dbuser;
my $dbpassword;
my $dbconnect;

#~ Check if we have some particular argument to start a cheat function
if ($#ARGV >= 0){
	$cheat = $ARGV[0];
} else {
	$cheat = undef;
}

#~ Check if there's still a process instance and eventually start/stop/restart it
&CheckStartRestart();

#~ Controlling paths for the daemon process, configuration
&CtrlLogDir();
&CtrlConfDir();

#~ Loading configuration from file
&LoadConf();

$proc = &Daemonize();

#~ Write Pid Information
if (!$error) {
	if (-e $pid2check) {
		&LogMessage("$file : PID File $pid2check already exists. Exiting");
		exit(0);
	} else {
		unless (open (FILE, $pidfile)) {
			$error = "Error opening PID file $pidfile for writing " . $!;
			exit(0);
		}
		&LogMessage("$file : PID $proc : Writing pid information to $pidfile");
		print FILE $proc . "\n";
		close (FILE);
	}
}

#~ Main loop of Daemons
while (&CheckRunning()){
	if (not $processManager){
		$processManager = new Parallel::ForkManager($maxMainProcesses);
		$processManager->run_on_finish(
			sub {
				my($pid, $ident) = @_;
				&LogMessage("Process $ident is died wid PID: $pid");
				&LogMessage("Process $ident is died wid PID: $pid", $logfile);
				&LogMessage("Process $ident is died wid PID: $pid", $errorlogfile);
			}
		);
		$processManager->run_on_start(
			sub {
				my($pid, $ident) = @_;
				&LogMessage("Process $ident is started with PID: $pid");
				&LogMessage("Process $ident is started with PID: $pid", $logfile);
				&LogMessage("Process $ident is started with PID: $pid", $errorlogfile);
			}
		);
		$processManager->run_on_wait();
		foreach $child (0 .. $#MainProcesses){
			my $ident = $MainProcesses[$child];
			my $pid = $processManager->start($MainProcesses[$child]) and next;
			if ($MainProcesses[$child] eq "terminalServer"){
				&CreateServerSocket();
				$processManager->finish($child);
			} elsif ($MainProcesses[$child] eq "driverManager"){
				&StartDriverManager();
				$processManager->finish($child);
			} elsif ($MainProcesses[$child] eq "resultManager"){
				&StartResultManager();
				$processManager->finish($child);
			}
		}
		$processManager->wait_all_children();
	}
}

&LogMessage("$file : PID $proc : END", $logfile);
&LogMessage("$file : PID $proc : END", $errorlogfile);
&LogMessage("$file : PID $proc : END", $accesslogfile);
&LogMessage("$file : PID $proc : END");

exit(0);

sub Daemonize(){
	$pid = fork();
    exit(0) if $pid; #Exit if $pid exists (parent)
	setsid(); #As Child
	$proc = $$;
	return ($proc);
}

sub CreateServerSocket(){
	&LogMessage("Bringing up Socket server interface for command line", $logfile);
	$hostserver = terminalServer->new();
	&LogMessage("Setting TerminalServer to port $port logging in $accesslogfile", $logfile);
	$hostserver->run(host=>'localhost', port=>$port, log_level=>'3', log_file=>$accesslogfile);
}

sub LogMessage(){
	my ($message, $dest) = @_;
	print "MSG->$message\tDST->$dest\n" if($debug);
	if (defined $dest){
		my $outfile = $syslogdir."/".$dest;
		open(OUT, ">>$outfile");
		print OUT localtime()." $message\n";
		close(OUT);
	} else {
		print localtime() . " $message\n";
	}
}

sub CtrlLogDir(){
	unless(chdir "/"){
		&LogMessage("$file: Can't chdir to /: $!");
	}
	unless(umask 0){
		&LogMessage("$file: Unable to umask 0");
	}
	if ( -d $syslogdir){
		&LogMessage("$file: Log Directory present") ;
		my $startMessage = "-" x 80 ."\n".localtime()."\t STARTING / RESTARTING POLIFEMO DAEMON\n".localtime()." "."-" x 80 ."\n";
		&LogMessage($startMessage, $logfile);
		&LogMessage($startMessage, $errorlogfile);
		&LogMessage($startMessage, $accesslogfile);
	} else {
		&LogMessage("$file: Warning: Log Directory NOT present");
		mkdir $syslogdir or &LogMessage($!);
		&LogMessage("$file: Created $syslogdir");
	}

}

sub CtrlConfDir(){
	if (-d $sysctrldir){
		&LogMessage("Config path found->$sysctrldir", $logfile);
	} else {
		&LogMessage("Config path NOT found: $sysctrldir\t Creating new one!", $logfile);
		mkdir $sysctrldir or &LogMessage($!, $errorlogfile);
		&LogMessage("Created -> $sysctrldir", $logfile);
	}
	if (-e $configfile){
		&LogMessage("Config file found-> $configfile", $logfile);
	} else {
		&LogMessage("Config file NOT found: $configfile\t Creating new one!", $logfile);
		&LogMessage("Config file NOT found: $configfile\t Creating new one!", $errorlogfile);
		#~ Setting Global settings
		$cfgPolifemo->param("site.title", $file);
		&LogMessage("Setting title to ->$file", $logfile);
		$cfgPolifemo->param("site.debug", 0);
		&LogMessage("Setting debug to -> 0", $logfile);
		#~ Setting Variables for main process
		$cfgPolifemo->param("site.pidfile", $pidfile);
		&LogMessage("Setting pidfile to ->$pidfile", $logfile);
		$cfgPolifemo->param("site.pid2check", $pid2check);
		&LogMessage("Setting pid2check to ->$pid2check", $logfile);
		#~ Setting Variables for default logging path
		$cfgPolifemo->param("logging.syslogdir", $syslogdir);
		&LogMessage("Setting syslogdir to ->$syslogdir", $logfile);
		$cfgPolifemo->param("logging.logfile", $logfile);
		&LogMessage("Setting logfile to ->$logfile", $logfile);
		$cfgPolifemo->param("logging.errorlogfile", $errorlogfile);
		&LogMessage("Setting errorlogfile to ->$errorlogfile", $logfile);
		$cfgPolifemo->param("logging.accesslogfile", $accesslogfile);
		&LogMessage("Setting accesslogfile to ->$accesslogfile", $logfile);
		#~ Setting Variables for default Server interface
		$cfgPolifemo->param("server.host", "localhost");
		&LogMessage("Setting Server host to ->localohst", $logfile);
		$cfgPolifemo->param("server.port", "5000");
		&LogMessage("Setting Server port to -> 5000", $logfile);
		#~ Setting Variables for DBMS Server interface
		$cfgPolifemo->param("database.type", "Pg");
		&LogMessage("Setting Database type to ->Pg", $logfile);
		$cfgPolifemo->param("database.host", "localhost");
		&LogMessage("Setting Database host to ->localhost", $logfile);
		$cfgPolifemo->param("database.port", "5432");
		&LogMessage("Setting Database port to ->5432", $logfile);
		$cfgPolifemo->param("database.schema", "politest");
		&LogMessage("Setting Database schema to ->politest", $logfile);
		$cfgPolifemo->param("database.user", "polifemo");
		&LogMessage("Setting Database user to ->polifemo", $logfile);
		$cfgPolifemo->param("database.password", "polifemo");
		&LogMessage("Setting Database password to ->polifemo", $logfile);
		#~ Writing informations to file
		$cfgPolifemo->write($configfile);
		&LogMessage("Configuration written", $logfile);
		&LoadConf();
	}
}

sub LoadConf(){
	&LogMessage("Loading / Reloading configuration file $configfile", $logfile);
	&LogMessage("Loading / Reloading configuration file $configfile", $errorlogfile);
	$config = new Config::Simple($configfile);
	#~ Loading Global settings
	$file = $config->param("site.title");
	&LogMessage("Loading Title-> $file", $logfile);
	$debug = $config->param("site.debug");
	&LogMessage("Loading Debug-> $debug", $logfile);
	#~ Loading Variables for main process
	$pidfile = $config->param("site.pidfile");
	&LogMessage("Loading PidFile-> $pidfile", $logfile);
	$pid2check = $config->param("site.pid2check");
	&LogMessage("Loading Pid2Check-> $pid2check", $logfile);
	#~ Loading Variables for default logging path
	$syslogdir = $config->param("logging.syslogdir");
	&LogMessage("Loading Logging Dir-> $syslogdir", $logfile);
	$logfile = $config->param("logging.logfile");
	&LogMessage("Loading Logging file-> $logfile", $logfile);
	$errorlogfile = $config->param("logging.errorlogfile");
	&LogMessage("Loading Error file-> $errorlogfile", $logfile);
	$accesslogfile = $config->param("logging.accesslogfile");
	&LogMessage("Loading Access file-> $accesslogfile", $logfile);
	#~ Loading Variables for default Server interface
	$hostserver = $config->param("server.host");
	&LogMessage("Loading Server host-> $hostserver", $logfile);
	$port = $config->param("server.port");
	&LogMessage("Loading Server port-> $port", $logfile);
	#~ Loading Variables for default Database interface
	$dbtype = $config->param("database.type");
	&LogMessage("Loading Database type-> $dbtype", $logfile);
	$dbhost = $config->param("database.host");
	&LogMessage("Loading Database host-> $dbhost", $logfile);
	$dbport = $config->param("database.port");
	&LogMessage("Loading Database port-> $dbport", $logfile);
	$dbschema = $config->param("database.schema");
	&LogMessage("Loading Database schema-> $dbschema", $logfile);
	$dbuser = $config->param("database.user");
	&LogMessage("Loading Database user-> $dbuser", $logfile);
	$dbpassword=$config->param("database.password");
	&LogMessage("Loading Database password->$dbpassword", $logfile);
}

sub CheckRunning(){
	if (-e $pid2check){
		open RUN, $pid2check;
		@pidrun = <RUN>;
		close RUN;
	}
	return $pidrun[0] if ($pidrun[0]);
	return(0);
}

sub CheckStartRestart(){
#~ Controlling if the process is still running and eventually restart
	if ($cheat ne ""){
		&LogMessage("Requested action: $cheat");
		&LogMessage("Requested action: $cheat", $logfile);
		&LogMessage("Requested action: $cheat", $errorlogfile);
	}
	if (-e $pid2check){
		&LogMessage("$file : PID File $pid2check already exists.");
		$oldpid = &CheckRunning();
		if ($oldpid ne "0"){
			&LogMessage("Found another instance of the service with PID-> $oldpid  Restarting....", $errorlogfile);
			&LogMessage("Found another instance of the service with PID-> $oldpid  Restarting....", $logfile);
			kill '-KILL', $oldpid;
			&LogMessage("Killing the instance $oldpid", $errorlogfile);
			&LogMessage("Killing the instance $oldpid", $logfile);
			unlink $pid2check;
		}
	}
}
